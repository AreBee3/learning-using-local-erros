# Deep supervised learning using local errors
The code implements all the expriments reported in the [Deep Supervised Learning Using Local Errors](https://arxiv.org/abs/1711.06756) paper. The expriments contrast the performance of 3 learning methods:
1. Standard backpropagation
2. [Feedback alignment](https://www.nature.com/articles/ncomms13276). Feedback alignment usese random matrices to backpropagate the errors during the backward pass as opposed to the transpose of the forward weight matrices used in standard backpropagation
3. Local error learning which is the main contribution of the [Deep Supervised Learning Using Local Errors](https://arxiv.org/abs/1711.06756) paper. A local random classifier generates error locally in each layer. This error is used to update the layer's weights. No errors are received from higher layers.

The experiments were carried out using three networks solving the MNIST and CIFAR10 classification tasks. The commands for running all the experiments are collected in the 5 files: `mnist_experiments`, `cifar10_convnet_experiments_1`, `cifar10_convnet_experiments_2`, `cifar10_vgg_experiments_1`, and `cifar10_vgg_experiments_2`. The MNIST and CIFAR10 dataset files are downloaded and placed in the './data' folder. Experiment results are saved in the './runs' folder. 

## Installation
The code makes use of pytorch and torchvision. Install the required python packages using:
```
pip install -r requirements.txt
```

## Usage
```
python main.py [-h] [--batch-size BATCH_SIZE]
               [--test-batch-size TEST_BATCH_SIZE] [--epochs EPOCHS]
               [--nTrials NTRIALS] [--start-lr START_LR] [--momentum MOMENTUM]
               [--model {mnist_mlp,cifar10_convnet,cifar10_VGG}] [--cpu]
               [--log-interval LOG_INTERVAL]
               [--fix-initial-layers FIX_INITIAL_LAYERS]
               [--feedback-alignment] [--local-learning]
               [--gaussian-classifier-weights] [--trainable-local-classifier]
               [--sign-aligned-local-feedback] [--no-dropout]
               [--binary-classifier-weights]

Learning using local errors and learning using feedback alignment experiments

optional arguments:
  -h, --help            show this help message and exit
  --batch-size BATCH_SIZE
                        input batch size for training (default: 100)
  --test-batch-size TEST_BATCH_SIZE
                        input batch size for testing (default: 10000)
  --epochs EPOCHS       number of epochs to train (default: 30)
  --nTrials NTRIALS     number of training trials (default: 20)
  --start-lr START_LR   Initial learning rate. This learning rate will be cut
                        down by a factor of 5 at epochs 25, 50, and 75
                        (default: 0.01)
  --momentum MOMENTUM   momentum term used in gradient descent (default: 0.9)
  --model {mnist_mlp,cifar10_convnet,cifar10_VGG}
                        network model to use (default: mnist_mlp)
  --cpu                 disables CUDA training and runs training on CPU
  --log-interval LOG_INTERVAL
                        how many batches to wait before logging training
                        status
  --fix-initial-layers FIX_INITIAL_LAYERS
                        how many early layers to fix. Parameters of fixed
                        layers are fixed at initializaion and do not change
                        during learning (default : 0)
  --feedback-alignment  Train using feedback alignment (default: False)
  --local-learning      Train using local error learning (default: False)
  --gaussian-classifier-weights
                        Use random local classifier weights that are drawn
                        from a Gaussian distribution. If not specified, a
                        uniform distribution is used. Only applicable if
                        --local-learning is used (default: False)
  --trainable-local-classifier
                        Train the local classifier weights. Only applicable
                        when --local-learning is used (default: False)
  --sign-aligned-local-feedback
                        Sign-aligned or sign-concordant error feedback from
                        the local classifier. Using symmetric feedback if not
                        specified. Only applicable if --local-learning is used
                        (default: False)
  --no-dropout          Disable dropout during training (default: False)
  --binary-classifier-weights
                        Make the local classifier weights binary. Only
                        applicable if --local-learning is used (default:
                        False)
```