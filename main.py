import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
import torchvision
from torchvision import transforms,datasets
import argparse
import os
import models
import numpy as np
import subprocess

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        outputs = model(data)
        loss = 0
        all_losses = []
        for output in outputs:
            loss += F.cross_entropy(output, target)
            all_losses.append(loss.item())
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), all_losses))

def test(args, model, device, test_loader,data_name):
    model.eval()
    test_loss = 0
    correct = 0
    test_loss = [0]
    correct = [0]
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            outputs = model(data)
            for i,output in enumerate(outputs):
                if i== len(test_loss):
                   test_loss.extend([0])
                   correct.extend([0])
                test_loss[i] += F.cross_entropy(output, target, size_average=False).item() # sum up batch loss
                pred = output.max(1, keepdim=True)[1] # get the index of the max log-probability
                correct[i] += pred.eq(target.view_as(pred)).sum().item()

    test_loss = np.array(test_loss)
    correct = np.array(correct)
    test_loss /= len(test_loader.dataset)
    print('\n{} : Average loss: {}, Accuracy: {}/{} ({}%)\n'.format(data_name,
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return correct


def save_test_results(state, filename='run_results'):
    """Saves run results to disk"""
    directory = "runs/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = directory + filename
    torch.save(state, filename)

parser = argparse.ArgumentParser(description='Learning using local errors and learning using feedback alignment experiments')
parser.add_argument(
    '--batch-size', type=int, default=100, help='input batch size for training (default: 100)')
parser.add_argument(
    '--test-batch-size', type=int, default=1000,  help='input batch size for testing (default: 10000)')
parser.add_argument('--epochs', type=int, default=30,  help='number of epochs to train (default: 30)')
parser.add_argument('--nTrials', type=int, default=20, help='number of training trials (default: 20)')

parser.add_argument('--start-lr', type=float, default=0.01,  help='Initial learning rate. This learning rate will be cut down by a factor of 5 at epochs 25, 50, and 75  (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.9,  help='momentum term used in gradient descent (default: 0.9)')
parser.add_argument('--model', type=str, choices = ['mnist_mlp','cifar10_convnet','cifar10_VGG'],default='mnist_mlp',  help='network model to use (default: mnist_mlp)')
parser.add_argument('--cpu', action='store_true', default=False, help='disables CUDA training and runs training on CPU')
parser.add_argument('--log-interval', type=int, default=50,  help='how many batches to wait before logging training status')
parser.add_argument('--fix-initial-layers', type=int, default=0,  help='how many early layers to fix. Parameters of fixed layers are fixed at initializaion and do not change during learning (default : 0) ')
parser.add_argument('--feedback-alignment', action='store_true', default=False, help='Train using feedback alignment (default: False)')
parser.add_argument('--local-learning', action='store_true', default=False, help='Train using local error learning (default: False)')
parser.add_argument('--gaussian-classifier-weights', action='store_true', default=False, help='Use random local classifier weights that are drawn from a Gaussian distribution. If not specified, a uniform distribution is used. Only applicable if --local-learning is used  (default: False)')
parser.add_argument('--trainable-local-classifier', action='store_true', default=False, help='Train the local classifier weights. Only applicable when --local-learning is used (default: False)')
parser.add_argument('--sign-aligned-local-feedback', action='store_true', default=False, help='Sign-aligned or sign-concordant error feedback from the local classifier.  Using symmetric feedback if not specified. Only applicable if --local-learning is used  (default: False)')
parser.add_argument('--no-dropout', action='store_true', default=False, help='Disable dropout during training  (default: False)')
parser.add_argument('--binary-classifier-weights', action='store_true', default=False, help='Make the local classifier weights binary. Only applicable if --local-learning is used  (default: False)')



def adjust_learning_rate(args,optimizer, epoch):
    """Sets the learning rate to the initial LR divided by 5 at 60th, 120th and 160th epochs"""
    lr = args.start_lr * ((0.2 ** int(epoch >= 25)) * (0.2 ** int(epoch >= 50))* (0.2 ** int(epoch >= 75)))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def get_gpu_memory_usage():
    result = subprocess.check_output(
        [
            'nvidia-smi', '--query-gpu=memory.used',
            '--format=csv,nounits,noheader'
        ])
    gpu_memory = [int(x) for x in result.decode('utf-8').strip().split('\n')]
    return gpu_memory
        
def main():
    args = parser.parse_args()
    

    args.cuda = not args.cpu and torch.cuda.is_available()
    if args.cuda:
        memory_load = get_gpu_memory_usage()
        cuda_device = np.argmin(memory_load).item()
        torch.cuda.set_device(cuda_device)
        device = torch.cuda.current_device()
    else:
        device = torch.device('cpu')

    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

    dataset = args.model.split('_')[0]
    if dataset == 'mnist':
        train_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, download=True,
                           transform=transforms.Compose([
                               transforms.ToTensor(),
                               transforms.Normalize((0.1307,), (0.3081,))
                           ])),batch_size=args.batch_size, shuffle=True,**kwargs)
        test_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=transforms.Compose([
                               transforms.ToTensor(),
                               transforms.Normalize((0.1307,), (0.3081,))
                           ])),batch_size=args.test_batch_size, shuffle=False,**kwargs)

    elif dataset == 'cifar10':

        normalize = transforms.Normalize(mean=[x/255.0 for x in [125.3, 123.0, 113.9]],
                                         std=[x/255.0 for x in [63.0, 62.1, 66.7]])

        transform_cifar10 = transforms.Compose([
                transforms.ToTensor(),
                normalize,
                ])

        train_loader = torch.utils.data.DataLoader(datasets.CIFAR10('./data', train=True, download=True,transform=transform_cifar10),
                      batch_size=args.batch_size, shuffle=True, **kwargs)
        test_loader = torch.utils.data.DataLoader(datasets.CIFAR10('./data', train=False, transform=transform_cifar10),
                      batch_size=args.batch_size, shuffle=False, **kwargs)

    elif dataset == 'svhn':
        transform = transforms.Compose([
                        transforms.ToTensor(),
                        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])


        train_loader = torch.utils.data.DataLoader(datasets.SVHN('./data', split = 'train',download=True, transform=transform),
                                                  batch_size=args.batch_size,
                                                  shuffle=True,
                                                  **kwargs)

        test_loader = torch.utils.data.DataLoader(datasets.SVHN('./data', split = 'test',download=True, transform=transform),
                                                  batch_size=args.batch_size,
                                                  shuffle=False,
                                                  **kwargs)
        


    #LR_multiplier = (args.end_lr / args.start_lr)**(1.0 / (args.epochs - 1))

    learning_kwargs = {'enable_fb_alignment' : args.feedback_alignment,
                    'enable_local_learning' : args.local_learning,
                    'trainable_local_weight' : args.trainable_local_classifier,
                       'binary_classifier_weights' : args.binary_classifier_weights,
                       'gaussian' : args.gaussian_classifier_weights,
                       'fix_initial_layers' : args.fix_initial_layers,
                       'no_dropout' : args.no_dropout,                          
                          'sign_aligned_local_weight' : args.sign_aligned_local_feedback}

    standard_bp = not(learning_kwargs['enable_fb_alignment'] or learning_kwargs['enable_local_learning'])
    save_file_name = args.model + '-' + '-'.join(['standard_bp'] if standard_bp else [x for x,y in learning_kwargs.items() if y])
    if args.fix_initial_layers > 0:
        save_file_name += '-'+repr(args.fix_initial_layers)
    test_performance = []
    train_performance = []
    for trial in range(1,args.nTrials+1):
        print(learning_kwargs)
        print('In trial {} of {}'.format(trial,args.nTrials))
        model = models.__dict__[args.model](**learning_kwargs)

        if args.cuda:
            model.cuda()
        print(model)

        optimizer = optim.SGD(filter(lambda x : x.requires_grad,model.parameters()), nesterov = True,lr=args.start_lr, momentum=0.9, weight_decay=0.0)        

        test_performance.append([])
        train_performance.append([])        
        for epoch in range(1, args.epochs + 1):
            adjust_learning_rate(args,optimizer, epoch)
            
            print('learning rate: {}'.format(optimizer.param_groups[0]['lr']))
            train(args, model, device, train_loader, optimizer, epoch)
            correct_test = test(args, model, device, test_loader,' test set ' )
            correct_train = test(args, model, device, train_loader,' train set ' )            

            test_performance[-1].append(correct_test)
            train_performance[-1].append(correct_train)            
            save_test_results({'test' : test_performance,'train' : train_performance,'model_desc': repr(model),'state_dict' : model.state_dict()},filename = save_file_name)

            
if __name__ == '__main__':
    main()            
