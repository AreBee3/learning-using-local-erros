import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn as nn
import torch.nn.functional as F
from learning_extensions import fb_alignment_hook,local_learning_layer,fix_layer_params


class mnist_mlp(nn.Module):

    def __init__(self,enable_fb_alignment = False,enable_local_learning = False, trainable_local_weight = False,
                 sign_aligned_local_weight = False,binary_classifier_weights = False,gaussian= False,no_dropout = False,fix_initial_layers = 0):
        super(mnist_mlp, self).__init__()
        assert not(enable_local_learning and enable_fb_alignment), "feedback alignment and local learning can not be simultaneously enabled"
        self.enable_local_learning = enable_local_learning        
        self.no_dropout = no_dropout
        
        self.fc1 = nn.Linear(784,1000,bias = False)
        self.fc2 = nn.Linear(1000,1000,bias = False)
        self.fc3 = nn.Linear(1000,1000,bias = False)


        self.bn1 = nn.BatchNorm1d(1000)
        self.bn2 = nn.BatchNorm1d(1000)
        self.bn3 = nn.BatchNorm1d(1000)


        linear_conv_layers = [self.fc1,self.fc2,self.fc3]
        assert fix_initial_layers <= len(linear_conv_layers),'fixing more layers than there are layers in the network'

        for layer in linear_conv_layers[:fix_initial_layers]:
            fix_layer_params(layer)

        
        self.local1 = self.local2 = self.local3  = lambda x: (x,None)

        if enable_local_learning:
           self.local1 = local_learning_layer(1000,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local2 = local_learning_layer(1000,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local3 = local_learning_layer(1000,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
        else:
           self.fc4 = nn.Linear(1000,10,bias = False)                                        
           self.bn4 = nn.BatchNorm1d(10)        

        if enable_fb_alignment:
           fb_alignment_hook(self.fc2)
           fb_alignment_hook(self.fc3)
           fb_alignment_hook(self.fc4)                          
            

    def forward(self, x):
        if self.no_dropout:
            dropout = lambda x,p,training: x
        else:
            dropout = lambda x,p,training: F.dropout(x,p=p,training=training)            


        x = x.view(-1,784)
        x = dropout(x,0.2,self.training)
        x = F.relu(self.bn1(self.fc1(x)))
        x,classifier1 = self.local1(x)

        x = dropout(x,0.3,self.training)
        x = F.relu(self.bn2(self.fc2(x)))
        x,classifier2 = self.local2(x)

        x = dropout(x,0.4,self.training)
        x = F.relu(self.bn3(self.fc3(x)))
        x,classifier3 = self.local3(x)

           
        if not(self.enable_local_learning):
           x = dropout(x,0.2,self.training)
           x = self.bn4(self.fc4(x))
           
        if self.enable_local_learning:
           return [classifier1,classifier2,classifier3]
        else:
           return [x]



class cifar10_convnet(nn.Module):
      
    def __init__(self,enable_fb_alignment = False,enable_local_learning = False, trainable_local_weight = False,
                 sign_aligned_local_weight = False,binary_classifier_weights = False,no_dropout = False,gaussian= False,fix_initial_layers = 0):
        super(cifar10_convnet, self).__init__()
        self.enable_local_learning = enable_local_learning
        self.no_dropout = no_dropout
        
        assert not(enable_local_learning and enable_fb_alignment), "feedback alignment and local learning can not be simultaneously enabled"
        
        
        self.conv1 = nn.Conv2d(3, 96, 5,padding = 2)
        self.conv2 = nn.Conv2d(96, 128, 5,padding = 2)
        self.conv3 = nn.Conv2d(128, 256, 5,padding = 2)         

        self.fc1 = nn.Linear(256*4*4, 2048)
        self.fc2 = nn.Linear(2048, 2048)

        linear_conv_layers = [self.conv1,self.conv2,self.conv3,self.fc1,self.fc2]
        assert fix_initial_layers <= len(linear_conv_layers),'fixing more layers than there are layers in the network'

        for layer in linear_conv_layers[:fix_initial_layers]:
            fix_layer_params(layer)

        
        self.bnconv1 = nn.BatchNorm2d(96)
        self.bnconv2 = nn.BatchNorm2d(128)
        self.bnconv3 = nn.BatchNorm2d(256)              

        self.bnfc1 = nn.BatchNorm1d(2048)
        self.bnfc2 = nn.BatchNorm1d(2048)

        self.local_conv1 = self.local_conv2 = self.local_conv3 = \
                           self.local_fc1 = self.local_fc2 = lambda x: (x,None)
        if enable_local_learning:
           self.local_conv1 = local_learning_layer(96*16*16,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local_conv2 = local_learning_layer(128*8*8,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local_conv3 = local_learning_layer(256*4*4,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local_fc1 = local_learning_layer(2048,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
           self.local_fc2 = local_learning_layer(2048,10,trainable = trainable_local_weight,sign_aligned = sign_aligned_local_weight,binary_classifier_weights = binary_classifier_weights,gaussian = gaussian)
        else:
            self.fctop = nn.Linear(2048,10,bias = False)                                        
            self.bntop = nn.BatchNorm1d(10)             

        if enable_fb_alignment:
           fb_alignment_hook(self.conv2)
           fb_alignment_hook(self.conv3)

           fb_alignment_hook(self.fc1)
           fb_alignment_hook(self.fc2)
           fb_alignment_hook(self.fctop)           

        
        
    def forward(self, x):
        if self.no_dropout:
            dropout = lambda x,p,training: x
        else:
            dropout = lambda x,p,training: F.dropout(x,p=p,training=training)            

        x = dropout(x,p=0.1,training=self.training)
        x = F.max_pool2d(F.relu(self.bnconv1(self.conv1(x))), 3,stride = 2,ceil_mode=True)
        x,classifier1 = self.local_conv1(x)

        x = dropout(x,p=0.25,training=self.training)
        x = F.max_pool2d(F.relu(self.bnconv2(self.conv2(x))), 3,stride = 2,ceil_mode=True)
        x,classifier2 = self.local_conv2(x)
        
        x = dropout(x,p=0.25,training=self.training)
        x = F.max_pool2d(F.relu(self.bnconv3(self.conv3(x))), 3,stride = 2,ceil_mode=True)
        x,classifier3 = self.local_conv3(x)
        
        x = x.view(-1,256*4*4)
        x = dropout(x,p=0.25,training=self.training)
        
        x = F.relu(self.bnfc1(self.fc1(x)))
        x,classifier4 = self.local_fc1(x)
        
        x = dropout(x,p=0.5,training=self.training)     
        x = F.relu(self.bnfc2(self.fc2(x)))     
        x,classifier5 = self.local_fc2(x)

        if not(self.enable_local_learning):
            x = dropout(x,p=0.2,training=self.training)             
            x = self.bntop(self.fctop(x))

        if self.enable_local_learning:
           return [classifier1,classifier2,classifier3,classifier4,classifier5]
        else:
           return [x]
       
svhn_convnet = cifar10_convnet


##VGG definition
class cifar10_VGG(nn.Module):
    class DummyDropout(nn.Module,):
        def __init__(self,dummy_p):
            super(cifar10_VGG.DummyDropout,self).__init__()
        def forward(self,x):
            return x
        
    def __init__(self,  num_classes=10, enable_fb_alignment = False,enable_local_learning = False, trainable_local_weight = False,
                 sign_aligned_local_weight = False,binary_classifier_weights = False,no_dropout = False,gaussian = False,fix_initial_layers = 0):
        super(cifar10_VGG, self).__init__()
        assert not(enable_local_learning and enable_fb_alignment), "feedback alignment and local learning can not be simultaneously enabled"
        cfg = [64, 'L',0.5,64, 'M','L', 128,'L',0.5, 128, 'M','L', 256,'L',0.5, 256, 'M','L', 512,'L', 0.5, 512, 'M','L', 0.5, 512,'L', 512, 'M','L',0.5]
        
        self.enable_local_learning =  enable_local_learning
        self.trainable_local_weight = trainable_local_weight
        self.sign_aligned_local_weight = sign_aligned_local_weight
        self.binary_classifier_weights = binary_classifier_weights
        self.gaussian = gaussian
        
        self.dropoutClass = cifar10_VGG.DummyDropout if no_dropout else nn.Dropout
        
        self.features = self.make_layers(cfg,batch_norm = True)
        linear_conv_layers = [x for x in self.features if isinstance(x,nn.Linear) or isinstance(x,nn.Conv2d)]
        assert fix_initial_layers <= len(linear_conv_layers),'fixing more layers than there are layers in the network'
        for layer in linear_conv_layers[:fix_initial_layers]:
            fix_layer_params(layer)

        
        if enable_fb_alignment:
            for layer in [layer for layer in self.modules() if isinstance(layer,nn.Conv2d)]:
                fb_alignment_hook(layer)

        self.local_learning_layers = [layer for layer in self.modules() if isinstance(layer,local_learning_layer)]
        if not(enable_local_learning):
            self.classifier = nn.Sequential(
                self.dropoutClass(0.2),
                nn.Linear(512,10),
                nn.BatchNorm1d(10)
            )
        
    def make_layers(self,cfg, batch_norm=False):
        layers = []
        in_channels = 3
        dim = 32
        for v in cfg:
            if v == 'M':
                layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
                dim /= 2
            elif v == 'L':
                if self.enable_local_learning:
                    layers += [local_learning_layer(dim*dim*in_channels,10,return_classifier_out = False, trainable = self.trainable_local_weight,sign_aligned = self.sign_aligned_local_weight,
                                                    binary_classifier_weights = self.binary_classifier_weights,gaussian = self.gaussian)]
            elif type(v) is int:
                conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
                if batch_norm:
                    layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
                else:
                    layers += [conv2d, nn.ReLU(inplace=True)]
                in_channels = v
            else:
                layers += [self.dropoutClass(v)]
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.features(x)

        if self.enable_local_learning:
            classifiers = [x.classifier_out for x in self.local_learning_layers]
            return classifiers
        else:
            x = x.view(x.size(0), -1)
            x = self.classifier(x)
            return [x]



